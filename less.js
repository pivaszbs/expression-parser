import Relation from "./relation";

export default class Less extends Relation {
    calculate() {
        return Number(this.left.calculate() < this.right.calculate());
    }
    
}