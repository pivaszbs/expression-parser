import Expression from './expression';

export default class Binary extends Expression {
    constructor(left, right) {
        super();
        this.left = left;
        this.right = right;
    }
}