import Term from "./term";

export default class Plus extends Term {
    calculate() {
        return this.left.calculate() + this.right.calculate()
    }
}