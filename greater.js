import Relation from "./relation";

export default class Greater extends Relation {
    calculate() {
        return Number(this.left.calculate() > this.right.calculate());
    }
}