import Term from "./term";

export default class Factor extends Term {
    calculate() {
        return this.left.calculate() * this.right.calculate();
    }
}
