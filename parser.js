import Less from "./less";
import Equal from "./equal";
import Greater from './greater';
import Minus from './minus';
import Multiply from "./multiply";
import Plus from './plus';
import Integer from './integer';
import Divide from './divide';

export default class Parser {
    parse(input) {
        return this.parseRelation(input);
    }

    parseRelation(str) {
        let check = this.checker(str);
        for (let i = 0; i < str.length; i++) {
            let par = check();
            if (par === 0) {
                switch (str[i]) {
                    case '<':
                        return new Less(
                            this.parseTerm(str.substring(0, i)), 
                            this.parseTerm(str.substring(i + 1))
                        );

                    case '>':
                        return new Greater(
                            this.parseTerm(str.substring(0, i)),
                            this.parseTerm(str.substring(i + 1))
                        );
                    
                    case '=':
                        return new Equal(
                            this.parseTerm(str.substring(0, i)),
                            this.parseTerm(str.substring(i + 1))
                        );
                }
            }
        }
        return this.parseTerm(str);
    }

    checker(str) {
        let i = 0;
        let par = 0;
        return () => {
            if (str[i] === '(') {
                par++;
            }
            else if (str[i] === ')'){
                par--;
            }
            i++;
            return par;
        }
    };

    parseTerm(str) {
        let check = this.checker(str);
        for (let i = 0; i < str.length; i++) {
            let par = check();
            if (par === 0) {
                switch (str[i]) {
                    case '+':
                        return new Plus(
                            this.parseFactor(str.substring(0, i)),
                            this.parseTerm(str.substring(i + 1))
                        );
                    case '-':
                        return new Minus(
                            this.parseFactor(str.substring(0, i)),
                            this.parseTerm(str.substring(i + 1))
                        );
                    default:
                        continue;
                }
            }
        }

        return this.parseFactor(str);
    }

    parseFactor(str) {
        let check = this.checker(str);
        for (let i = 0; i < str.length; i++) {
            let par = check();
            if (par === 0)
                switch (str[i]) {
                    case '*':
                        return new Multiply(
                            this.parsePrimary(str.substring(0, i)),
                            this.parseFactor(str.substring(i + 1))
                        );

                    case '/':
                        return new Divide(
                            this.parsePrimary(str.substring(0, i)),
                            this.parseFactor(str.substring(i + 1))
                        );
                    
                    default:
                        continue;
                }
        }

        return this.parsePrimary(str);
    }

    parsePrimary(str) {
        if (
            str[0] === '(' &&
            str[str.length - 1] === ')'
        )
            return this.parse(str.substring(1, str.length - 1));
        else {
            return new Integer(Number(str));
        }
    }
}