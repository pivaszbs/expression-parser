import Relation from "./relation";

export default class Equal extends Relation {
    calculate() {
        return this.left.calculate() === this.right.calculate();
    }
    
}