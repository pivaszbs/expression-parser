import Term from "./term";

export default class Multiply extends Term {
    calculate() {
        return this.left.calculate() / this.right.calculate()
    }
}