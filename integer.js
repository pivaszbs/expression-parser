import Primary from "./primary";

export default class Integer extends Primary {
    constructor (i) {
        super(i);
        this.i = this.value;
    }

    calculate() {
        return this.i;
    }
}