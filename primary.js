import Expression from "./expression";

export default class Primary extends Expression {
    constructor (v) {
        super();
        this.value = v;
    }
}