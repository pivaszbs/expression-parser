import Primary from "./primary";

export default class Parenthesized extends Primary {
    constructor (e) {
        super(e);
        this.expression = this.value;
    }

    calculate() {
        return this.expression.calculate();
    }
}