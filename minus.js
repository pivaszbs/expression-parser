import Term from "./term";

export default class Minus extends Term {
    calculate() {
        return this.left.calculate() - this.right.calculate()
    }
}