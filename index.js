import Parser from './parser';

const test = (input) => {
    const parser = new Parser();
    const expression = parser.parse(input);
    console.log(expression.calculate() === Number(eval(input)));
}

test('3+4*8');
test('3+4*8+(24+36)*15');
test('3+4*8-(1+10+12)/15>28');
test('12*4-15*3/2<12');